# Getting started

This chapter is a walk trough how you can setup and build your first **snui** application.

## Creating a project

1. Creating a new Rust projet

```rust
cargo new snui-app
```

2. Adding snui dependencies in Cargo.toml

```rust
[dependencies]
snui = { git="https://gitlab.com/snakedye/snui.git" }
snui-wayland = { git="https://gitlab.com/snakedye/snui.git" }
snui-derive = { git="https://gitlab.com/snakedye/snui.git" }
```

- **snui** is the core library with the widgets.
- **snui-wayland** is an implementation of a wayland backend to run **snui** is a wayland environment.
- **snui-derive** is the crate for proc-macros useful in snui.

> **NOTE**: Currently, **snui-wayland** is the only existing display backend but ideally one could choose in the future what backend they would like to support.

3. A minimal example

#### `[main.rs]`

```rust
use snui::{utils::*, widgets::*, *};
use snui_wayland::*;

fn ui() -> impl Widget<()> {
	default_window(Label::from("Window"), Label::from("Hello World!"))
}

fn main() {
    WaylandClient::init((), |client, qh| {
        let mut view = client.create_window(ui(), qh);
        view.set_title("Example".to_string());
    })
}
```

## How to color

We'll walk through the `color.rs` example in `snui-wayland/examples/` as it touches many elements of snui that are helpful or necessary to build responsive applications.

### Application state

```rust
struct Color {
    color: tiny_skia::Color,
}
```

In this example, the application state is simply the color we are editing.

### Lenses

Since most of the widgets in the library are not aware of the application state, we need to implement Lenses to gives them access to data stored inside it.

> **NOTE**: For a more in-depth explanation of lenses, visit [Lens]().

To edit the a channel of the color with the slider we need to implement `Lens<f32, ColorChannel>` with the `ColorChannel` being the message. 

```rust
enum ColorChannel {
    Blue,
    Red,
    Green,
    Alpha,
}

impl Lens<f32, ColorChannel> for Color {
    fn with<V, F: FnOnce(&f32) -> V>(&self, message: &ColorChannel, f: F) -> V {
        let value = match *message {
            ColorChannel::Alpha => self.color.alpha(),
            ColorChannel::Red => self.color.red(),
            ColorChannel::Green => self.color.green(),
            ColorChannel::Blue => self.color.blue(),
        };
        f(&value)
    }
    fn with_mut<V, F: FnOnce(&mut f32) -> V>(&mut self, message: &ColorChannel, f: F) -> V {
        let mut data = self.with(message, |f| *f);
        let value = f(&mut data);
        match *message {
            ColorChannel::Alpha => self.color.set_alpha(data),
            ColorChannel::Red => self.color.set_red(data),
            ColorChannel::Green => self.color.set_green(data),
            ColorChannel::Blue => self.color.set_blue(data),
        }
        value
    }
}
```

Each message represent a color channel the lens gives the widgets access to. 

Our application also needs to display the color in [hex]() format so we'll need to implement `Lens<String, ()>` in order for labels to fetch the name of the color.

```rust
impl Lens<String, ()> for Color {
    fn with<V, F: FnOnce(&String) -> V>(&self, _message: &(), f: F) -> V {
        let fmt = format!("{:#010X}", self.color.to_color_u8().get()).replace("0x", "#");
        f(&fmt)
    }

    fn with_mut<V, F: FnOnce(&mut String) -> V>(&mut self, _message: &(), f: F) -> V {
        f(&mut String::new())
    }
}
```

### Custom widget

For our color editor to be more fun, it would be helpful to see what our color looks like. For this purpose, we will create a custom widget that will display it.

```rust
struct ColorBlock {
    rect: Rectangle,
	...
}
```

> **NOTE**: Some details about the widget are omitted to simplify the example.

Each time the slider changes a channel of the color in the application state, the `ColorBlock` widget will be updated to reflect that change. 

Let's see what the widget implementation looks that looks like:

```rust
impl Widget<Color> for ColorBlock {
    fn draw_scene(&mut self, scene: Scene, _env: &Env) {
		...
        scene.draw(&self.rect)
    }
    fn update(&mut self, ctx: &mut UpdateContext<Color>, _env: &Env) -> Damage {
        ctx.prepare();
        self.rect.set_texture(ctx.color);
		self.rect.damage()
    }
    fn event(&mut self, ctx: &mut UpdateContext<Color>, event: Event, _: &Env) -> Damage {
        ...
        Damage::None
    }
    fn layout(
        &mut self,
        _ctx: &mut LayoutContext<Color>,
        _bc: &BoxConstraints,
        _env: &Env,
    ) -> Size {
        self.rect.size()
    }
}
```

- **event**: Since no event will cause the color block to change, we can simply return that there is no damage.

- **update**: The color block fetches the color in the application, sets in a rectangle and return its damage state.

- **layout**: The widget size is fixed so no need to layout anything.

- **draw_scene**: The rectangle with the color inside the state is drawn.

### Building the ui

Now that that foundations are layed out, the widget tree can be built.

```rust
fn sliders() -> impl Widget<Color> {
    [
        ColorChannel::Red,
        ColorChannel::Green,
        ColorChannel::Blue,
        ColorChannel::Alpha,
    ]
    .into_iter()
    .map(|channel| {
        Padding::new(
            Slider::from(channel) 
                .class(match channel {
                    ColorChannel::Red => "accent-color-6",
                    ColorChannel::Green => "accent-color-2",
                    ColorChannel::Blue => "accent-color-3",
                    _ => "",
                })
                .with_fixed_height(8.)
                .anchor(START, CENTER),
        )
        .padding_top(5.)
        .padding_bottom(5.)
    })
    .collect::<Row<_, _>>()
}

fn content() -> impl Widget<Color> {
    Row!(
        Row!(
            DynLabel::from("")
                .class("important")
                .with_fixed_height(30.)
                .anchor(CENTER, START),
            ColorBlock {
                rect: Rectangle::new(200., 200.).radius(RelativeUnit::Percent(5.))
            }
            .flex(Orientation::Horizontal)
        )
        .clamp(),
        sliders().clamp()
    )
}

fn ui_builder() -> impl Widget<Color> {
	default_window(
        Label::from("Color").class("title"),
        content().clamp().padding(10.),
    );
}
```

> **NOTE**: For more details on how to build widgets, refer to the API [documentation]().

### Creating a window

```rust
fn main() {
    let color = Color {
        color: tiny_skia::Color::WHITE,
    };
    WaylandClient::init(color, |client, qh| {
        let mut view = client.create_window(ui_builder(), qh);
		...
    })
}
```
