# Environments

The environment is an object that holds data that doesn't necessarily fit inside the application
and can be dynamically fetched.

It can be notably used to store configuration options or the [stylesheet]() of the application.

## Definition 

The `Environment` trait is defined as is

```rust
pub trait Environment {
    fn get(&self, list: List<Id>, property: &str) -> Option<Value>;
}
```

You might notice that this trait depends on three key types. `List`, `Id` and `Value`.

### `Value`

```rust
pub enum Value<'a> {
    Unit(RelativeUnit),
    Texture(Texture),
    Bool(bool),
    String(String),
    Str(&'a str),
    Int(i32),
    Usize(usize),
}
```

`Value` is the most straight forward. It's essentially an enum with all the types `get` can return.

One can also return directly type held inside the value by using `try_get` from `EnvironmentExt` as defined here.  

```rust
    fn try_get<'a, T>(&'a self, list: List<Id>, property: &str) -> Option<T>
    where
        T: TryFrom<Value<'a>>,
        T::Error: std::fmt::Debug,
    {
        Environment::get(self, list, property).and_then(|value| value.try_into().ok())
    }
```

> **NOTE**: EnvironmentExt is implemented for all Environments.

### `Id`

An `Id` is an object that serves to identify items. 

It's most commonly used inside **snui** to identify widgets for the purpose of theming but an `Id` can also represent a configuration option.

With the property, the Environment can determine what kind of data the `Id` is meant to represent and respond accordingly. 

### `List`

Last but not least, the `List`.

The `List` is not unique to the `Environment`. 

It's a _fifo_ simply linked list that requires no heap allocation as it lives on the stack. It is passed down the widget tree and grows as widgets append items to it.

To explore what is held in that list, one can use its iterator `ListIter` and traverse it to explore all the `Id`s that were appended.

> **NOTE**: The iteration goes from the most recent node to the least recent.

## Implementation

**snui** provides an `Environment` implementation in `ThemeManager`.