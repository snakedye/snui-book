# Widgets

A widget is a component of the UI represented by the `Widget` trait. **snui** provides a set of widgets and gives you the ability to build your own.

> **NOTE**: The widget trait has a generic parameter `T` that represent the type of the application. Most widgets take a generic type but a custom one for a specific application can have stricter requirements.

## Pipeline

The sequence of "events" in **snui** looks somewhat like this

```
event > update > layout > draw
``` 

> **NOTE**: There might be exceptions, especially with layouting, where this order is not followed.

- **event**: Your application will receive an `Event` from the display backend (`snui-wayland`) and a context with a mutable reference to the application state. That event is passed down the widget tree to widgets that would need it. Once a widget receives an event, it then has the responsibility to tell if it was damaged or not by returning `Damage`.

- **update**: If the application state was mutated, the display backend will update the state of the widget system to reflect the changes that occurred due to the event. Once a widget has processed an update, it then has the responsibility to tell if it was damaged or not by returning `Damage`.

- **layout**: Layouting can happen independently but usually it follows an event or an update where damage is necessary. Widgets are passed dimension constraints and can return a size that fits those constraints.

- **draw**: This is where your application is drawn, usually the damage will be incremental.

## Compositing

Most widgets are not meant to handle layouting or theming on their own. 

For example, a label doesn't know how to center itself in a box or have padding. This can be solved by wrapping the label in a `Padding` and an `WidgetBox` like this.

### Default
```rust
let label = WidgetBox::new(Padding::new(Label::from("hello")).padding_top(10.));
```

### WidgetExt
```rust
let label = Label::from("hello").padding_top(10.).clamp();
```

> **NOTE**: Both `padding_top` and `clamp` are defined in the trait [WidgetExt]() which is implemented for all widgets.