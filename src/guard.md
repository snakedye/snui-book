# Guards and the Guarded trait

A guard is a type that keeps track of the mutation of some inner data.

They are used in **snui** to determine when the `data` was mutated and schedule an `update` and evaluate if a widget has been damaged and required a redraw.

## Definition

It is expressed in **snui** through the `Guarded` trait.

```rust
pub trait Guarded {
    fn touched(&self) -> bool;
    fn restore(&mut self);
}
```

- `touched` : returns true if the data has been tampered.
- `restore` : puts the object in an untouched state. 

## Implementation

The main implementation of the `Guarded` trait is `MutGuard`.

```rust
pub struct MutGuard<'a, T> {
    inner: T,
    gbool: Gbool<'a>,
}
```

`MutGuard` holds a type `T` and a `Gbool`.

The gbool keeps track of the mutation of the inner data and has the nice property of being "_borrowable_". 
This essentially means that multiple guards can borrow the same `Gbool`.

## Macros
**snui** provide multiple macros that ease the use of guards.

### `map!`
[map!]() will.. map a field of the inner type of a `MutGuard` to another one.

```rust
let guard = Guard::new((1, 10)); // contains: (1, 10)
let mapped = map!(guard, 1); // contains: &mut 10
```

### `set!`
[set!]() is similar to map with the exception that assigns a value to the _mapped_ field.

```rust
struct Pair {
	a: u32,
	b: u32
}

let pair = Pair { a: 1, b: 10 };
let guard = Guard::new(pair); // Pair { a: 1, b: 10 }
set!(guard, b = 7);  // Pair { a: 1, b: 7 }
```

### `[Guarded]`

This proc macro will derive an implementation of `Guarded`.

```rust
#[derive(Guarded)]
struct SuperRect(Rectangle);
```

> **NOTE**: It only works if all the fields implement `Guarded` too.

### `[guard]`

This proc macro will wrap all the fields of the type in a `Guard` and generate an implementation of `Guarded`.

```rust
#[guard]
struct Pair(u32, u32); // Pair(Guard<u32>, Guard<u32>)
```


