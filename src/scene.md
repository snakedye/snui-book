# Scene

The `Scene` is a high level representation of the graphical state of a node in the widget tree.

It is create by the display backend before the draw and is passed down the widget tree. Widgets can create new instances from the `Scene` to update its state.

## Background

One notable utility of the `Scene` is to keep track of the background. It holds a list of all the backgrounds applied on a node in order to accurately damage the application.

```rust
pub struct Background<'b> {
    depth: usize,
    background: &'b dyn DynBackground,
    position: Cow<'b, Position>,
}
```

A background is comprised of a [DynBackground](), a position and a depth.

A `DynBackground` is essentially a drawable object with a texture. The position is the absolute position of that object if the graph and the depth is z-axis ordering of said `Texture`.

> **NOTE**: The depth determines whether items exist on the same plane. Items that belong to the same plane will be rendered with their predecessors during damage and only `Backgrounds` with inferior depth will be cleared. 

## Drawing

The most straight forward way to draw on the canvas is to implement the [Drawable]() trait.

```rust
pub trait Drawable: Geometry {
    fn draw(&self, ctx: &mut DrawContext, transform: Transform);
}
```

`Drawable` objects are given a `DrawContext` which gives access to the drawing `Backend` and helpers methods to damage it.

> **NOTE**: `Drawable` requires the type to implement the [Geometry]() trait.

### Implementation

The `Drawable` object one will probably interact the most is the `Rectangle`.

```rust
pub struct Rectangle {
    width: f32,
    height: f32,
    texture: Texture,
    radius: [RelativeUnit; 4],
}

impl Drawable for Rectangle {
    fn draw(&self, ctx: &mut DrawContext, transform: tiny_skia::Transform) {
        ctx.draw(|ctx, pb| {
            let radius = self.radius.map(|r| r.as_abs(self.min_side()));
            let path = Rectangle::path(pb, self.width.get(), self.height.get(), radius).unwrap();
            super::fill_path(&path, &self.texture, ctx.deref_mut(), self, transform);
            path
        })
    }
}
```