# Themeable

A [Themeable]() is an object, often a widget that can be configured by an `Environment`.

## Definition

```rust
pub trait Themeable {
    const NAME: &'static str;
    fn theme(&mut self, list: List<Id>, env: &dyn Environment);
}
```

`NAME` is the name of the item. It's what the `Environment` will get when accessing the item field of an `Id`.

The theme method implementation is where the item can fetch `Values` from the `Environment` corresponding to a property.

### Implementation

```rust
impl Themeable for Rectangle {
    const NAME: &'static str = "rectangle";
    fn theme(&mut self, list: utils::List<Id>, env: &dyn Environment) {
        if let Some(radius) = env.try_get(list, "radius") {
            self.radius.set([radius; 4]);
        }
        if let Some(radius) = env.try_get(list, "top-left-radius") {
            self.set_top_left_radius(radius);
        }
        if let Some(radius) = env.try_get(list, "top-right-radius") {
            self.set_top_right_radius(radius);
        }
        if let Some(radius) = env.try_get(list, "bottom-right-radius") {
            self.set_bottom_right_radius(radius);
        }
        if let Some(radius) = env.try_get(list, "bottom-left-radius") {
            self.set_bottom_left_radius(radius);
        }
        if let Some(texture) = env.try_get::<Texture>(list, "background") {
            self.set_texture(texture)
        }
    }
}
```

## Cheatsheet

### `rectangle`/`progress`/`toggle`/`window`
property	|	Description		| Value
:-----------|------------------|:---------:
radius	|	The radius of all corners	|	RelativeUnit
top-left-radius	|	The radius of the top left corner	| RelativeUnit
top-right-radius	|	The radius of the top right corner	|	RelativeUnit
bottom-left-radius	|	The radius of the bottom left corner	|	RelativeUnit
bottom-right-radius	|	The radius of the bottom right corner	|	RelativeUnit
background	| The texture of the rectangle	|	Texture
border-texture*	| The texture of the border	|	Texture
border-width*	| The width of the border	|	Float

### `label`
property	|	Description		| Value
:-----------|------------------|:---------:
foreground	|	The color of the text	|	Texture
font-family	|	The name of the font	|	Str/String
font-style	|	The style of a font (Bold, Regular, Light, Italic)	| Str/String
font-size	|	The size of the font	|	Float

### `style`
property	|	Description		| Value
:-----------|------------------|:---------:
radius	|	The radius of all corners	|	RelativeUnit
top-left-radius	|	The radius of the top left corner	| RelativeUnit
top-right-radius	|	The radius of the top right corner	|	RelativeUnit
bottom-left-radius	|	The radius of the bottom left corner	|	RelativeUnit
bottom-right-radius	|	The radius of the bottom right corner	|	RelativeUnit
background	| The texture of the background	|	Texture
border-texture	| The texture of the border	|	Texture
border-width	| The width of the border	|	Float

### `circle`
property	|	Description		| Value
:-----------|------------------|:---------:
background	| The texture of the rectangle	|	Texture
border-texture*	| The texture of the border	|	Texture
border-width*	| The width of the border	|	Float

### `padding`
property	|	Description		| Value
:-----------|------------------|:---------:
padding	| The padding around the entire widget	| Float
padding-left	|	The padding at the left of the widget	| Float
padding-right	|	The padding at the right of the widget	| Float
padding-top		|	The padding above the widget	| Float
padding-bottom	|	The padding below the widget	| Float

### `slider`
property	|	Description		| Value
:-----------|------------------|:---------:
radius	|	The radius of all corners	|	RelativeUnit
top-left-radius	|	The radius of the top left corner	| RelativeUnit
top-right-radius	|	The radius of the top right corner	|	RelativeUnit
bottom-left-radius	|	The radius of the bottom left corner	|	RelativeUnit
bottom-right-radius	|	The radius of the bottom right corner	|	RelativeUnit
background	| The background of the slider	|	Texture
higlight	| The texture of the slider	|	Texture

### `handle`
property	|	Description		| Value
:-----------|------------------|:---------:
background	| The texture of the handle	|	Texture
ratio	|	The ratio between the size of the handle and the size of the slider	|	Float

### `header`
property	|	Description		| Value
:-----------|------------------|:---------:
background	| The background of the window header	|	Texture

### `box`
property	|	Description		| Value
:-----------|------------------|:---------:
min-width	|	The minimum width of the widget	|	RelativeUnit
min-height	|	The minimum height of the widget	|	RelativeUnit
max-width	|	The maximum width of the widget	|	RelativeUnit
max-height	|	The maximum height of the widget	|	RelativeUnit
width	|	The width of the widget	|	RelativeUnit
height	|	The height of the widget	|	RelativeUnit

### Classes

A few items in **snui** push `Ids` have special classes that represent the state of the application in that specific region of the widget tree.

- `activated` : This class can be found with the `window` item. It is set when the window is used.

- `hovered`	: This class is set with the item `button` by the `Button` widget when it is "active".

- `activated` : This class is set with the item `activate` by the `Activate` widget when it is "active".

> NOTE: If these classes are not defined, you can assume the item is not the state the class advertise. Meaning that a button that doesn't have the class `hovered` is definitely not hovered.